resource "aws_s3_bucket" "main" {
  bucket = "vallen-${var.customer}-data-lake-${var.environment}"
  acl    = "private"
  region = var.region

  versioning {
    enabled = false
  }

  tags = {
    Name         = "DataLake"
    Environment  = var.environment
    Organization = var.customer
  }
}

resource "aws_s3_bucket_public_access_block" "main" {
  bucket              = aws_s3_bucket.main.id
  block_public_acls   = true
  block_public_policy = true
}

resource "aws_s3_bucket_object" "keys" {
  for_each = toset(list(
    "inputs/csv/",
    "outputs/orc/",
    "outputs/query/"
  ))

  bucket                 = aws_s3_bucket.main.id
  acl                    = "private"
  key                    = each.value
  content_type           = "application/x-directory"
  server_side_encryption = "aws:kms"
  kms_key_id             = var.kms_key_arn
}

# Athena

resource "aws_athena_database" "main" {
  name          = var.customer
  bucket        = aws_s3_bucket.main.bucket
  force_destroy = true
  encryption_configuration {
    encryption_option = "SSE_KMS"
    kms_key           = var.kms_key_id
  }
}

resource "aws_athena_workgroup" "main" {
  name = "${var.customer}-${var.environment}-wg"

  configuration {
    enforce_workgroup_configuration    = true
    publish_cloudwatch_metrics_enabled = true

    result_configuration {
      output_location = "s3://${aws_s3_bucket.main.bucket}/outputs/query/"

      encryption_configuration {
        encryption_option = "SSE_KMS"
        kms_key_arn       = var.kms_key_arn
      }
    }
  }

  tags = {
    Name         = "Athena WorkGroup"
    Environment  = var.environment
    Organization = var.customer
    Area         = "DataLake"
  }
}

data "aws_iam_policy_document" "athena" {
  statement {
    effect  = "Allow"
    actions = ["athena:*"]
    resources = [
      "${aws_athena_workgroup.main.arn}",
    ]
  }
}

resource "aws_iam_policy" "athena" {
  name        = "${var.customer}_athena"
  path        = "/"
  description = "Athena policy for ${var.customer}"
  policy      = data.aws_iam_policy_document.athena.json
}

# ETL Role

data "aws_iam_policy_document" "bastion" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.aws_bastion_account_id}:root"]
    }
  }
}

resource "aws_iam_role" "etl" {
  name               = "${var.customer}_data_lake_etl"
  assume_role_policy = data.aws_iam_policy_document.bastion.json

  tags = {
    Name         = "ETL IAM Role"
    Environment  = var.environment
    Organization = var.customer
    Area         = "Security"
  }
}

data "aws_iam_policy_document" "etl" {
  statement {
    effect = "Allow"

    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation",
    ]

    resources = [
      "${aws_s3_bucket.main.arn}",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "s3:PutObject*",
      "s3:DeleteObject*",
      "s3:AbortMultipartUpload",
      "s3:ListMultipartUploadParts"
    ]

    resources = [
      "${aws_s3_bucket.main.arn}",
      "${aws_s3_bucket.main.arn}/inputs/*",
    ]
  }
}

resource "aws_iam_role_policy" "etl" {
  name   = "Vallen${title(var.customer)}${title(var.environment)}ETLRolePolicy"
  role   = aws_iam_role.etl.id
  policy = data.aws_iam_policy_document.etl.json
}