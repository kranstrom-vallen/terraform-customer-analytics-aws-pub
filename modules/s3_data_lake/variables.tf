variable "region" {}
variable "environment" {}
variable "customer" {}
variable "aws_bastion_account_id" {}

variable "kms_key_id" {}
variable "kms_key_arn" {}