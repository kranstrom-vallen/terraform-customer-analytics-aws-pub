# S3 Data Lake

The S3 Data Lake module creates:

- Private/locked S3 bucket and objects
- Athena DB & workgroup
- ETL (write-only) role
