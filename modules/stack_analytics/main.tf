# KMS Key

resource "aws_kms_key" "main" {
  description             = "KMS Key for ${var.customer} - ${var.environment}"
  deletion_window_in_days = 10

  tags = {
    Organization = var.customer
    Area         = "security"
    Environment  = var.environment
  }
}

resource "aws_kms_alias" "main" {
  name          = "alias/${var.customer}-${var.environment}"
  target_key_id = aws_kms_key.main.key_id
}

# QuickSight Groups

resource "aws_quicksight_group" "main" {
  for_each = toset(list(
    "reader",
    "author"
  ))
  group_name     = "${var.customer}-${var.environment}-${each.value}"
  aws_account_id = var.account_id
  description    = "${each.value} group for ${var.customer}"
  namespace      = "default"
}

# Data Lake

module "data_lake" {
  source                 = "../s3_data_lake"
  customer               = var.customer
  environment            = var.environment
  aws_bastion_account_id = var.aws_bastion_account_id
  region                 = var.region
  kms_key_id             = aws_kms_key.main.key_id
  kms_key_arn            = aws_kms_key.main.arn
}