terraform {
  backend "s3" {
    key            = "account/bi/dev/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-statelock"
  }

  required_version = "> 0.12.0"
}