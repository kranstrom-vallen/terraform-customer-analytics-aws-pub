variable "region" {
  default = "us-east-1"
}

variable "organization" {}
variable "environment" {
  default = "dev"
}

variable "terraform_role_arn" {}
variable "aws_bastion_account_id" {}