data "aws_caller_identity" "current" {}

# Customer Specific Stacks

module "stack_customer_1" {
  source                 = "../../modules/stack_analytics"
  account_id             = data.aws_caller_identity.current.account_id
  environment            = var.environment
  region                 = var.region
  customer               = "cust"
  aws_bastion_account_id = var.aws_bastion_account_id
}