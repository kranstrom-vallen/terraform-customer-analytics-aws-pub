# QuickSight Setup

## Setup QuickSight on AWS Account

1. Go to QuickSight and Sign-up for an Enterprise Account
2. Disable auto-discovery to be explicit about enabling any sources
3. Enable **Athena** by going to QuickSight -> **Manage QuickSigt** (top right corner) -> **Security & permissions**.
4. Check and uncheck **Athena**, then go through selecting the S3 bucket(s) that need access to.
5. Given that KMS is used for encryption, add KMS key to QuickSight group using command:

```
$ aws kms create-grant --key-id <KMS key ARN> --grantee-principal <Your Amazon QuickSight Role ARN> --operations Decrypt
```

## Add users to groups

```
# List & Describe Group
$ aws quicksight list-groups \
    --aws-account-id=111122223333 --namespace=default
$ aws quicksight describe-group \
    --aws-account-id=11112222333 --namespace=default \
    --group-name=<group-name>

-- Add user to group
$ aws quicksight create-group-membership \
    --aws-account-id=111122223333 --namespace=default \
    --group-name=<group-name> --member-name=<member>
```



## References

- [AWS: Athena Permissions](https://docs.aws.amazon.com/quicksight/latest/user/troubleshoot-athena-insufficient-permissions.html)
- [AWS: QuickSight Groups](https://docs.aws.amazon.com/quicksight/latest/user/managing-users.html#creating-quicksight-groups)