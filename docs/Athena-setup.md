# Athena

## CSV -> ORC

To save on costs with Athena, we want to switch from CSV to ORC. ORC changes the data to columnar storage and compresses it.

```
-- OPTIONAL: DROP existing table if exists
-- DROP TABLE ${athena-db}.cust_sales_data_csv;

-- Create external table mapping to S3 key containing CSV-files
CREATE EXTERNAL TABLE ${athena-db}.cust_sales_data_csv (
         `SITE_CD` string,
         `ORDER_NBR` string,
         `LINE_NBR` INT,
         `NPS` string,
         `IREF` string,
         `DESC_1` string,
         `DESC_2` string,
         `UM_CD` string,
         `QTY_SHIPPED` DECIMAL,
         `UNIT_PRICE` DECIMAL,
         `PO_NBR` string,
         `CUST_NAME` string,
         `CUST_ID` string,
         `INVOICE_DATE` TIMESTAMP
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
LOCATION 's3://${s3-bucket}/inputs/csv/sales_data/';

-- OPTIONAL: DROP existing table if exists
DROP TABLE ${athena-db}.cust_sales_data;

-- Create table that changes format to ORC from CSV
CREATE TABLE ${athena-db}.cust_sales_data
WITH (
      external_location = 's3://${s3-bucket}/outputs/orc/sales_data/',
      format = 'ORC')
AS SELECT *
FROM ${athena-db}.cust_sales_data_csv;
```