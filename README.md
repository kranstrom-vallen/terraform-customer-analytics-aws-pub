# Terraform Customer Analytics (AWS)

The Terraform Customer Analytics (AWS) provides the setup for a customer-specific analytics environment using **Amazon QuickSight (Enterprise)** and a Data Lake backend using **S3** and **Athena**.


## Requirements

- QuickSight Enterprise
- HashiCorp Terraform
- (Recommended) aws-vault

The setup in this repo assumes the use of a "Bastion AWS" account, which is the use of the variable `aws_bastion_account_id`

## Setup

### Remote State

Remote State setup documentation information is available under `remote-state/Administrator.md`

### Setting up environemnts

Go (or create) environment directory and then issue:

```
# Initialize
$ aws-vault exec <profile> -- terraform init --backend-config="bucket=<remote-state-bucket-name>"

# Plan
$ aws-vault exec <profile> -- terraform plan -out deployit

# Deploy (if things look good!)
$ aws-vault exec <profile> -- terraform apply deployit
```

### Automatic Documentation

`terraform graph | dot -Tsvg > graph.svg`

## Disclaimer

**THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.**